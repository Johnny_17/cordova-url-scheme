cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.lampa.startapp/www/startApp.js",
        "id": "com.lampa.startapp.startapp",
        "pluginId": "com.lampa.startapp",
        "merges": [
            "startApp"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.lampa.startapp": "0.1.4",
    "cordova-plugin-whitelist": "1.2.2",
    "com.napolitano.cordova.plugin.intent": "0.1.3"
}
// BOTTOM OF METADATA
});