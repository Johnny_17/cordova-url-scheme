cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.lampa.startapp/www/startApp.js",
        "id": "com.lampa.startapp.startapp",
        "pluginId": "com.lampa.startapp",
        "merges": [
            "startApp"
        ]
    },
    {
        "file": "plugins/cordova-plugin-device/www/device.js",
        "id": "cordova-plugin-device.device",
        "pluginId": "cordova-plugin-device",
        "clobbers": [
            "device"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.lampa.startapp": "0.1.4",
    "com.napolitano.cordova.plugin.intent": "0.1.3",
    "cordova-plugin-whitelist": "1.2.2",
    "cordova-plugin-device": "1.1.3-dev"
}
// BOTTOM OF METADATA
});